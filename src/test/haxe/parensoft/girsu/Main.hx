package parensoft.girsu;

import utest.Runner;
import utest.ui.Report;
import xmlpull.tests.*;

/**
 * ...
 * @author Parensoft.NET
 */
class Main{

  public function new() {}
    

  public static function main() {
    
    var runner = new Runner();

    // tests passed from 1.0
    runner.addCase(new TestFactory());
    runner.addCase(new TestSimple());
    runner.addCase(new TestSimpleWithNs());
    runner.addCase(new TestAttributes());
    runner.addCase(new TestEolNormalization());
    runner.addCase(new TestEvent());
    runner.addCase(new TestMisc());

    // tests never passed
    runner.addCase(new TestSimpleToken());
    runner.addCase(new TestEntityReplacement());

    // new tests fr m XmlPull 1.1.2
    runner.addCase(new TestCdsect());
    runner.addCase(new TestProcessDocdecl());
    runner.addCase(new TestSerialize());
    runner.addCase(new TestSerializeWithNs());
    runner.addCase(new TestSetInput());
    runner.addCase(new TestSimpleProcessDocdecl());
    runner.addCase(new TestToken());
    
    // girsu is not validating
    //runner.addCase(new TestSimpleValidation());
    

    // xml based tests - last
    //runner.addCase(new TestBootstrapXmlTests());
    //runner.addCase(new TestXmlCdsect());
    //runner.addCase(new TestXmlSimple());
    //runner.addCase(new TestXmlTypical());


    Report.create(runner);
    runner.run();
 
  }

    
    
  
}
