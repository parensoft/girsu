/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.girsu;

import xmlpull.*;

/**
 * ...
 * @author Parensoft.NET
 */
@:keep
class ParserFactory implements XmlPullParserFactory {
  
  private var features: Map<String, Bool> = new Map<String, Bool>();

  public function new() {
    
  }
  
  /* INTERFACE xmlpull.XmlPullParserFactory */
  
  public function setFeature(name:String, state:Bool):Void {
    if (name == XmlConstants.FEATURE_VALIDATION) throw "no validation supported";
    if ((state: Null<Bool>) == null) throw 'null value for feature $name';
    
    features[name] = state;
  }
  
  public function getFeature(name:String):Bool {
    if (features.exists(name)) return features[name];
    
    return false;
  }
  
  public function setNamespaceAware(awareness:Bool):Void {
    setFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES, awareness);
  }
  
  public function isNamespaceAware():Bool {
    return getFeature(XmlConstants.FEATURE_PROCESS_NAMESPACES);
  }
  
  public function setValidating(validating:Bool):Void {
    if (validating != false) throw "no validation supported";
  }
  
  public function isValidating():Bool {
    return false;
  }
  
  public function newPullParser():XmlPullParser {
    var parser = new Parser();
    
    for (feature in features.keys()) parser.setFeature(feature, features[feature]);
    
    return parser;
  }

  public function newSerializer():XmlSerializer {
    return new Serializer();
  }
  
}
