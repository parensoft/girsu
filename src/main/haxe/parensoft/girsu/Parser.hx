/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

/* Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The  above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE. */

// Contributors: Paul Hackenberger (unterminated entity handling in relaxed mode)
// Contributors: Jacek Szymanski (port to Haxe)

package parensoft.girsu;

import haxe.ds.Vector;
import haxe.io.Eof;
import haxe.io.Input;
import parensoft.eridu.CharsetRegistry;
import parensoft.eridu.charsets.Utf8;
import parensoft.eridu.io.BufferingReader;
import parensoft.eridu.io.EolNormalizingReader;
import parensoft.eridu.io.InputReader;
import parensoft.eridu.io.Reader;
import parensoft.girsu.util.Range;
import unifill.CodePoint;
import unifill.Unifill;
import xmlpull.XmlConstants;
import xmlpull.XmlEventType;
import xmlpull.XmlEventType.*;
import xmlpull.XmlPullParser;
import xmlpull.XmlPullParserException;

using StringTools;

/** A simple, pull based XML parser. This classe replaces the kXML 1
  XmlParser class and the corresponding event classes. */

class Parser implements XmlPullParser {

  var location: Dynamic; // Object
  static private inline var UNEXPECTED_EOF = "Unexpected EOF";
  static private inline var ILLEGAL_TYPE = "Wrong event type";
  static private inline var LEGACY = 999;
  static private inline var XML_DECL = 998;

  static private inline var NO_NAMESPACE = "";

  static private inline var BUF_SIZE = 8192;

  // general

  private var version: String;
  private var standalone: Null<Bool>;

  private var processNsp: Bool = false;
  private var relaxed: Bool = false;
  private var entityMap: Map<String, String>; 
  private var depth: Int;
  private var elementStack = new Vector<String>(16);
  private var nspStack = new Vector<String>(8);
  private var nspCounts = new Vector<Int>(4);
  private var defNsps = new Array<{level: Int, uri: String}>();

  // source

  private var reader: BufferingReader;
  private var encoding: String;
  private var srcBuf: Vector<Int>;

  private var srcPos: Int;
  private var srcCount: Int;

  private var line: Int;
  private var column: Int;

  // txtbuffer

  /** Target buffer for storing incoming text (including aggregated resolved entities) */
  private var txtBuf = new Vector<Int>(128);
  /** Write position  */
  private var txtPos: Int;

  // Event-related

  private var type: Int = XmlEventType.START_DOCUMENT;
  private var whitespace: Bool;
  private var namespace: String;
  private var prefix: String;
  private var name: String;

  private var degenerated: Bool;
//  private var attributeCount: Int;
  private var attribs = new Array<Attribute>();
  private var error: String;

  private var unresolved:  Bool;
  private var token: Bool;

  public function new() {

  }

  private function isProp(n1: String, prop: Bool, n2: String) {
    if (!n1.startsWith("http://xmlpull.org/v1/doc/"))
      return false;
    if (prop)
      return n1.substring(42) == n2;
    else
      return n1.substring(40) == n2;
  }

  private function adjustNsp(): Bool {

    var any = false;


    var i = 0;

    while (i < attribs.length) { 

      var attrName = attribs[i].name;
      var cut = attrName.indexOf(':');
      var prefix;

      if (cut != -1) {
        prefix = attrName.substring(0, cut);
        attrName = attrName.substring(cut + 1);
      }
      else if (attrName == "xmlns") {
        prefix = attrName;
        attrName = null;
      }
      else {
        i ++;
        continue;
      }


      if (!(prefix == "xmlns")) {
        any = true;
      }
      else if (attrName == null) {
        defNsps.push( { level: depth, uri: attribs[i].value } ); 
        attribs.splice(i, 1);
        i--;
      }
      else {
        var j = (nspCounts[depth]) << 1;
        nspCounts[depth] = nspCounts[depth] + 1;

        nspStack = ensureCapacity(nspStack, j + 2);
        nspStack[j] = attrName;
        nspStack[j + 1] = attribs[i].value;

        if (attrName != null && attribs[i].value == "")
          makeError("illegal empty namespace");

        attribs.splice(i, 1);
        i --;
      }

      i ++;
    }

    if (any) {
      var i = attribs.length - 1;
      while (i >= 0) {
      //for (int i = (attributeCount << 2) - 4; i >= 0; i -= 4) 

        var attrName = attribs[i].name;
        var cut = attrName.indexOf(':');

        if (cut == 0 && !relaxed)
          throw "illegal attribute name: " + attrName + " at " + this;

        else if (cut != -1) {
          var attrPrefix = attrName.substring(0, cut);

          attrName = attrName.substring(cut + 1);

          var attrNs = getNamespaceByPrefix(attrPrefix);

          if (attrNs == null && !relaxed)
            throw "Undefined Prefix: " + attrPrefix + " in " + this;

            
          attribs[i].nsUri = attrNs;
          attribs[i].prefix = attrPrefix;
          attribs[i].name = attrName;
          
          for (idx in (i + 1)...attribs.length)
            if (attribs[idx].nsUri == attribs[i].nsUri && attribs[idx].name == attribs[i].name)
              makeError('duplicate attribute {${attribs[i].nsUri}}${attribs[i].name}');
        }
        i --;
      }
    }

    var cut = name.indexOf(':');

    if (cut == 0)
      makeError("illegal tag name: " + name);

    if (cut != -1) {
      prefix = name.substring(0, cut);
      name = name.substring(cut + 1);
      this.namespace = getNamespaceByPrefix(prefix);
    }
    else {
      defNsps.length > 0 ? this.namespace = defNsps[defNsps.length - 1].uri : "";
    }


    if (this.namespace == null) {
      if (prefix != null)
        makeError("undefined prefix: " + prefix);
      this.namespace = NO_NAMESPACE;
    }

    return any;
  }

  @:generic
  private static function ensureCapacity<T>(arr: Vector<T>, required: Int): Vector<T> {
    if (arr.length >= required)
      return arr;
    var bigger = new Vector<T>(required + 16);
    Vector.blit(arr, 0, bigger, 0, arr.length);
    return bigger;
  }

  private function makeError(desc: String) {
    if (relaxed) {
      if (error == null)
        error = "ERR: " + desc;
    }
    else
      exception(desc);
  }

  private function exception(desc: String) {
    throw XmlPullParserException.newFromParserState(
        desc.length < 100 ? desc : desc.substring(0, 100) + "\n",
        this);
  }

  /** 
   * common base for next and nextToken. Clears the state, except from 
   * txtPos and whitespace. Does not set the type variable */

  private function nextImpl() {

    if (reader == null)
      exception("No Input specified");

    if (type == END_TAG) {
      depth--;
      if (defNsps.length > 0 && depth < defNsps[defNsps.length - 1].level) defNsps.pop();

    }

    while (true) {
      attribs = [];

      // degenerated needs to be handled before error because of possible
      // processor expectations(!)

      if (degenerated) {
        degenerated = false;
        type = END_TAG;
        if (defNsps.length > 0 && depth < defNsps[defNsps.length - 1].level) defNsps.pop();
        return;
      }


      if (error != null) {
        for (i in 0...error.length)
          push(error.charCodeAt(i));
        //				text = error;
        error = null;
        type = COMMENT;
        return;
      }


      //            if (relaxed
      //                && (stackMismatch > 0 || (peek(0) == -1 && depth > 0))) {
      //                int sp = (depth - 1) << 2;
      //                type = END_TAG;
      //                namespace = elementStack[sp];
      //                prefix = elementStack[sp + 1];
      //                name = elementStack[sp + 2];
      //                if (stackMismatch != 1)
      //                    error = "missing end tag /" + name + " inserted";
      //                if (stackMismatch > 0)
      //                    stackMismatch--;
      //                return;
      //            }

      prefix = null;
      name = null;
      namespace = null;
      //            text = null;

      type = peekType();

      switch (type) {

        case ENTITY_REF :
          pushEntity();
          return;

        case START_TAG :
          parseStartTag(false);
          return;

        case END_TAG :
          parseEndTag();
          if (defNsps.length > 0 && depth < defNsps[defNsps.length - 1].level) defNsps.pop();
          return;

        case END_DOCUMENT :
          return;

        case TEXT :
          pushText("<".code, !token);
          if (depth == 0) {
            if (whitespace)
              type = IGNORABLE_WHITESPACE;
            // make exception switchable for instances.chg... !!!!
            //	else 
            //    exception ("text '"+getText ()+"' not allowed outside root element");
          }
          return;

        default :
          type = parseLegacy(token);
          if (type != XML_DECL)
            return;
      }
    }
  }

  private function parseLegacy(ispush: Bool): Int {

    var req = "";
    var term;
    var result;
    var prev = 0;

    read(); // <
    var c: Int = read();
    
    if (c == "?".code) {
      if ((peek(0) == "x".code || peek(0) == "X".code)
          && (peek(1) == "m".code || peek(1) == "M".code)) {

            if (ispush) {
              push(peek(0));
              push(peek(1));
            }
            read();
            read();

            if ((peek(0) == "l".code || peek(0) == "L".code) && peek(1) <= " ".code) {

              if (line != 1 || column > 4) // wtf?
                makeError("PI must not start with xml");

              parseStartTag(true);

              if (attribs.length < 1 || !("version" == attribs[0].name))
                makeError("version expected");

              version = attribs[0].value;

              var pos = 1;

              if (pos < attribs.length
                  && "encoding" == attribs[pos].name) {
                    encoding = attribs[pos].value;
                    pos++;
                  }

              if (pos < attribs.length
                  && "standalone" == attribs[pos].name) {
                    var st = attribs[pos].value;
                    if ("yes" == st)
                      standalone = true;
                    else if ("no" == st)
                      standalone = true;
                    else
                      makeError("illegal standalone value: " + st);
                    pos++;
                  }

              if (pos != attribs.length)
                makeError("illegal xmldecl");

              whitespace = true;
              txtPos = 0;

              return XML_DECL;
            }
          }
          
      if (peek(0) == " ".code) throw new XmlPullParserException("<? cannot be followed by a space");

      /*            int c0 = read ();
                    int c1 = read ();
                    int */

      term = "?".code;
      result = PROCESSING_INSTRUCTION;
    }
    else if (c == "!".code) {
      if (peek(0) == "-".code) {
        result = COMMENT;
        req = "--";
        term = "-".code;
      }
      else if (peek(0) == "[".code) {
        result = CDSECT;
        req = "[CDATA[";
        term = "]".code;
        ispush = true;
      }
      else {
        result = DOCDECL;
        req = "DOCTYPE";
        term = -1;
      }
    }
    else {
      makeError("illegal: <" + c);
      return COMMENT;
    }

    for (i in 0...req.length)
      readVerify(req.charCodeAt(i));

    if (result == DOCDECL)
      parseDoctype(ispush);
    else {
      while (true) {
        c = read();
        if (c == -1){
          makeError(UNEXPECTED_EOF);
          return COMMENT;
        }

        if (ispush)
          push(c);

        if ((term == "?".code || c == term)
            && peek(0) == term
            && peek(1) == ">".code)
          break;

        prev = c;
      }

      if (term == "-".code && prev == "-".code && !relaxed)
        makeError("illegal comment delimiter: --->");

      read();
      read();

      if (ispush && term != "?".code)
        txtPos--;

    }
    return result;
  }

  /** precondition: &lt! consumed */

  private function parseDoctype(ispush: Bool): Void {

    var nesting = 1;
    var quoted = false;

    // read();

    while (true) {
      var i = read();
      switch (i) {

        case -1 :
          makeError(UNEXPECTED_EOF);
          return;

        case "\'".code :
          quoted = !quoted;

        case "<".code :
          if (!quoted)
            nesting++;

        case ">".code :
          if (!quoted) {
            if ((--nesting) == 0)
              return;
          }
      }
      if (ispush)
        push(i);
    }
  }

  /* precondition: &lt;/ consumed */

  private function parseEndTag(): Void {

    read(); // '<'
    read(); // '/'
    name = readName();
    skip();
    readVerify(">".code);

    var sp = (depth - 1) << 2;

    if (depth == 0) {
      makeError("element stack empty");
      type = COMMENT;
      return;
    }
    
    if (!relaxed) {
      if (!(name == elementStack[sp + 3])) {
        makeError("expected: /" + elementStack[sp + 3] + " read: " + name);

        // become case insensitive in relaxed mode

        //            int probe = sp;
        //            while (probe >= 0 && !name.toLowerCase().equals(elementStack[probe + 3].toLowerCase())) {
        //                stackMismatch++;
        //                probe -= 4;
        //            }
        //
        //            if (probe < 0) {
        //                stackMismatch = 0;
        //                //			text = "unexpected end tag ignored";
        //                type = COMMENT;
        //                return;
        //            }
      }

      namespace = elementStack[sp];
      prefix = elementStack[sp + 1];
      name = elementStack[sp + 2];
    }
  }

  private function peekType(): Int {
    switch (peek(0)) {
      case -1 :
        return END_DOCUMENT;
      case "&".code :
        return ENTITY_REF;
      case "<".code :
        switch (peek(1)) {
          case "/".code :
            return END_TAG;
          case "?".code , "!".code :
            return LEGACY;
          default :
            return START_TAG;
        }
      default :
        return TEXT;
    }
  }

  private function get(pos: Int) {
    var buf = [];
    for (idx in pos...txtPos) 
      buf.push(new CodePoint(txtBuf[idx]));

    return Unifill.uToString(buf);
    //return new String(txtBuf, pos, txtPos - pos);
  }

  /*
     private final String pop (int pos) {
     String result = new String (txtBuf, pos, txtPos - pos);
     txtPos = pos;
     return result;
     }
   */

  private function push(c: Int) {
	  
	if (c < " ".code && [ 9, 10, 13 ].indexOf(c) < 0) {
	  if (relaxed) return;
	  
	  makeError('character ${c} not allowed in xml');
	}

    whitespace = whitespace && (c <= " ".code);

    if (txtPos == txtBuf.length) {
      var bigger = new Vector<Int>(Math.ceil(txtPos * 4 / 3 + 4));
      Vector.blit(txtBuf, 0, bigger, 0, txtPos);
      txtBuf = bigger;
    }
	
    txtBuf[txtPos++] = c;
  }

  /** Sets name and attributes */

  private function parseStartTag(xmldecl: Bool): Void {

    if (!xmldecl)
      read();
    name = readName();
    attribs = [];

    while (true) {
      skip();

      var c = peek(0);

      if (xmldecl) {
        if (c == "?".code) {
          read();
          readVerify(">".code);
          return;
        }
      }
      else {
        if (c == "/".code) {
          degenerated = true;
          read();
          skip();
          readVerify(">".code);
          break;
        }

        if (c == ">".code && !xmldecl) {
          read();
          break;
        }
      }

      if (c == -1) {
        makeError(UNEXPECTED_EOF);
        //type = COMMENT;
        return;
      }

      var attrName = readName();

      if (attrName.length == 0) {
        makeError("attr name expected");
        //type = COMMENT;
        break;
      }
      for (idx in 0...attribs.length) {
        if (attrName == attribs[idx].name) makeError('duplicate attribute: $attrName');
      }

      attribs.push( { nsUri: "", prefix: null, name: attrName, value: null } );
      var i = attribs.length - 1;

      skip();

      if (peek(0) != "=".code) {
        if(!relaxed){
          makeError("Attr.value missing f. "+attrName);
        }
        attribs[i].nsUri = attrName;
      }
      else {
        readVerify("=".code);
        skip();
        var delimiter = peek(0);

        if (delimiter != "\'".code && delimiter != "\"".code) {
          if(!relaxed){
            makeError("attr value delimiter missing!");
          }
          delimiter = " ".code;
        }
        else 
          read();

        var p = txtPos;
        pushText(delimiter, true);

        attribs[i].value = get(p);
        txtPos = p;

        if (delimiter != " ".code)
          read(); // skip endquote
      }
    }

    var sp = depth++ << 2;

    elementStack = ensureCapacity(elementStack, sp + 4);
    elementStack[sp + 3] = name;

    if (depth >= nspCounts.length) {
      var bigger = new Vector<Int>(depth + 4);
      for (idx in 0...bigger.length) bigger[idx] = 0;
      Vector.blit(nspCounts, 0, bigger, 0, nspCounts.length);
      nspCounts = bigger;
    }

    nspCounts[depth] = nspCounts[depth - 1];

    /*
       if(!relaxed){
       for (int i = attributeCount - 1; i > 0; i--) {
       for (int j = 0; j < i; j++) {
       if (getAttributeName(i).equals(getAttributeName(j)))
       exception("Duplicate Attribute: " + getAttributeName(i));
       }
       }
       }
     */
    if (processNsp)
      adjustNsp();
    else
      namespace = "";

    elementStack[sp] = namespace;
    elementStack[sp + 1] = prefix;
    elementStack[sp + 2] = name;
  }

  /** 
   * result: isWhitespace; if the setName parameter is set,
   * the name of the entity is stored in "name" */

  private function pushEntity(): Void {

    push(read()); // &


    var pos = txtPos;

    while (true) {
      var c = peek(0);
      if (c == ";".code) {
        read();
        break;
      }
      if (c < 128
          && (c < "0".code || c > "9".code)
          && (c < "a".code || c > "z".code)
          && (c < "A".code || c > "Z".code)
          && c != "_".code
          && c != "-".code
          && c != "#".code) {
            if(!relaxed){
              makeError("unterminated entity ref");
            }

            trace("broken entitiy: "+get(pos-1));

            //; ends with:"+(char)c);           
            //                if (c != -1)
            //                    push(c);
            return;
          }

      push(read());
    }

    var code = get(pos);
    txtPos = pos - 1;
    if (token && type == ENTITY_REF){
      name = code;
    }

    if (code.charCodeAt(0) == "#".code) {
      var c =
        (code.charCodeAt(1) == "x".code
         ? Std.parseInt("0x" + code.substring(2))
         : Std.parseInt(code.substring(1)));
      push(c);
      return;
    }

    var result = entityMap.get(code);

    unresolved = result == null;

    if (unresolved) {
      if (!token)
        makeError("unresolved: &" + code + ";");
    }
    else {
      for (i in 0...result.length)
        push(result.charCodeAt(i));
    }
  }

  /** types:
    '<': parse to any token (for nextToken ())
    '"': parse to quote
    ' ': parse to whitespace or '>'
   */

  private function pushText(delimiter: Int, resolveEntities: Bool): Void {

    var next = peek(0);
    var cbrCount = 0;

    while (next != -1 && next != delimiter) { // covers eof, '<', '"'

      if (delimiter == " ".code)
        if (next <= " ".code || next == ">".code)
          break;

      if (next == "&".code) {
        if (!resolveEntities)
          break;

        pushEntity();
      }
      else if (next == "\n".code && type == START_TAG) {
        read();
        push(" ".code);
      }
      else
        push(read());

      if (next == ">".code && cbrCount >= 2 && delimiter != "]".code)
        makeError("Illegal: ]]>");

      if (next == "]".code)
        cbrCount++;
      else
        cbrCount = 0;

      next = peek(0);
    }
  }

  private function readVerify(c: Int): Void {
    var a = read();
    if (a != c)
      makeError("expected: '" + c + "' actual: '" + a + "'");
  }

  private function read(): Int {
    var result: Int;

    try {
      result = reader.readChar();
    }
    catch (e: Eof) {
      result = -1;
    }

    column++;

    if (result == "\n".code) {

      line++;
      column = 1;
    }

    return result;
  }

  /** Does never read more than needed */

  private function peek(pos: Int): Int {
    
    try {
      return reader[pos];
    }
    catch (e: Eof) {
      return -1;
    }

/*    while (pos >= peekCount) {

      var nw;

      try {
        nw = reader.readChar();
      }
      catch (e: Eof) {
        nw = -1;
      }
      
      if (nw == "\r".code) {
        wasCR = true;
        peekv[peekCount++] = "\n".code;
      }
      else {
        if (nw == "\n".code) {
          if (!wasCR)
            peekv[peekCount++] = "\n".code;
        }
        else
          peekv[peekCount++] = nw;

        wasCR = false;
      }
    }
    
    return peekv[pos];
*/  
  }

  private function readName(): String {

    var pos = txtPos;
    var c = peek(0);
    if ((c < "a".code || c > "z".code)

        && (c < "A".code || c > "Z".code)
        && c != "_".code
        && c != ":".code
        && c < 0x0c0
        && !relaxed)
      makeError("name expected");

    do {
      push(read());
      c = peek(0);
    }
    while ((c >= "a".code && c <= "z".code)
        || (c >= "A".code && c <= "Z".code)
        || (c >= "0".code && c <= "9".code)
        || c == "_".code
        || c == "-".code
        || c == ":".code
        || c == ".".code
        || c >= 0x0b7);

    var result = get(pos);
    txtPos = pos;
    return result;
  }

  private function skip(): Void {

    while (true) {
      var c = peek(0);
      if (c > " ".code || c == -1)
        break;
      read();
    }
  }

  //  public part starts here...

  public function setInput(input: Input): Void {
    // by default UTF-8
    if (input != null) {
      setReader(new InputReader(input, new Utf8()));
    }
    else {
      setReader(null);
    }
  }
  
  public function setInputWithEncoding(input: Input, anEncoding: String): Void {
    if (input == null) {
      throw new XmlPullParserException("null input stream and encoding specified");
    }
    else if (anEncoding == null) {
      setInput(input); // this is a hack until encoding detection is implemented.
    }
    else {
      setReader(new InputReader(input, CharsetRegistry.byName(anEncoding)));
      this.encoding = anEncoding;
    }
  }
  
  public function setReader(reader: Reader): Void {
    line = 1;
    column = 0;
    type = START_DOCUMENT;
    name = null;
    namespace = null;
    degenerated = false;
    attribs = [];
    encoding = null;
    version = null;
    standalone = null;
    prefix = null;


    srcPos = 0;
    srcCount = 0;
    depth = 0;
    
    nspCounts[0] = nspCounts[1] = nspCounts[2] = nspCounts[3] = 0;

    entityMap = new Map<String, String>();
    entityMap["amp"] = "&";
    entityMap["apos"] = "'";
    entityMap["gt"] = ">";
    entityMap["lt"] = "<";
    entityMap["quot"] = "\"";

    if (reader == null) {
      this.reader = null;
      return;
    }

    this.reader = new BufferingReader(new EolNormalizingReader(reader));

  }
    
    


  public function getFeature(feature: String): Bool {
    if (XmlConstants.FEATURE_PROCESS_NAMESPACES == feature)
      return processNsp;
    else if (isProp(feature, false, "relaxed"))
      return relaxed;
    else
      return false;
  }

  public function getInputEncoding(): String {
    return encoding;
  }

  public function defineEntityReplacementText(entity: String, value: String): Void {
    if (entityMap == null)
      throw "entity replacement text must be defined after setInput!";
    entityMap[entity] = value;
  }

  public function getProperty(property: String): Dynamic {
    if (isProp(property, true, "xmldecl-version"))
      return version;
    if (isProp(property, true, "xmldecl-standalone"))
      return standalone;
    if (isProp(property, true, "location"))            
      return location != null ? location : "unsupported"; // location
    return null;
  }

  public function getNamespaceCount(depth: Int): Int {
    if (depth > this.depth)
      throw 'new IndexOutOfBoundsException() $depth > ${this.depth}';
    
    return nspCounts[depth];
  }

  public function getNamespacePrefix(pos: Int): String {
    return nspStack[pos << 1];
  }

  public function getNamespaceUri(pos: Int): String {
    return nspStack[(pos << 1) + 1];
  }

  public function getNamespaceByPrefix(prefix: String): String {

    if ("xml" == prefix)
      return "http://www.w3.org/XML/1998/namespace";
    if ("xmlns" == prefix)
      return "http://www.w3.org/2000/xmlns/";

    var i =  (getNamespaceCount(depth) << 1) - 2;
    while (i >= 0) {
      if (prefix == null) {
        if (nspStack[i] == null)
          return nspStack[i + 1];
      }
      else if (prefix == nspStack[i])
        return nspStack[i + 1];
      i -= 2;
    }
    return null;
  }

  public function getDepth(): Int {
    return depth;
  }

  public function getPositionDescription(): String {

    var buf = new StringBuf();
    buf.add(XmlEventType.getName(type));
    buf.add(" ");

    if (type == START_TAG || type == END_TAG) {
      if (degenerated)
        buf.add("(empty) ");
      buf.add("<");
      if (type == END_TAG)
        buf.add("/");

      if (prefix != null)
        buf.add("{" + namespace + "}" + prefix + ":");
      buf.add(name);

      for (i in 0...attribs.length) {
        buf.add(" ");
        if (attribs[i].prefix != null)
          buf.add(
              "{" + attribs[i].nsUri + "}" + attribs[i].prefix + ":");
        buf.add(attribs[i].name + "='" + attribs[i].value + "'");
      }

      buf.add(">");
    }
    else if (type == IGNORABLE_WHITESPACE) {}
    else if (type != TEXT)
      buf.add(getText());
    else if (whitespace)
      buf.add("(whitespace)");
    else {
      var text = getText();
      if (text.length > 16)
        text = text.substring(0, 16) + "...";
      buf.add(text);
    }

    buf.add("@"+line + ":" + column);
    if(location != null){
      buf.add(" in ");
      buf.add(location);
    }
    else if(reader != null){
      buf.add(" in ");
      buf.add("unknown"); // reader.toString()
    }
    return buf.toString();
  }

  public function getLineNumber() {
    return line;
  }

  public function getColumnNumber() {
    return column;
  }

  public function isWhitespace() {
    if (type != TEXT && type != IGNORABLE_WHITESPACE && type != CDSECT)
      exception(ILLEGAL_TYPE);
    return whitespace;
  }

  public function getText() {
    return type < (TEXT :Int)
      || (type == ENTITY_REF && unresolved) ? null : get(0);
  }

  public function getNamespace() {
    return namespace;
  }

  public function getName() {
    return name;
  }

  public function getPrefix() {
    return prefix;
  }

  public function isEmptyElementTag() {
    if (type != START_TAG)
      exception(ILLEGAL_TYPE);
    return degenerated;
  }

  public function getAttributeCount() {
    if (type != START_TAG) return -1;
    
    return attribs.length;
  }

  public function getAttributeType(index: Int) {
    return "CDATA";
  }

  public function isAttributeDefault(index: Int) {
    return false;
  }

  public function getAttributeNamespace(index: Int) {
    if (index >= attribs.length)
      throw "IndexOutOfBoundsException()";
    return attribs[index].nsUri;
  }

  public function getAttributeName(index: Int) {
    if (index >= attribs.length)
      throw "IndexOutOfBoundsException()";
    return attribs[index].name;
  }

  public function getAttributePrefix(index: Int) {
    if (index >= attribs.length)
      throw "IndexOutOfBoundsException()";
    return attribs[index].prefix;
  }

  public function getAttributeValueByIndex(index: Int) {
    if (index >= attribs.length)
      throw "IndexOutOfBoundsException()";
    return attribs[index].value;
  }

  public function getAttributeValue(namespace: String, name: String) {

    for (i in Range.from(attribs.length - 1).by(-1).toIncl(0)) {
      //    for (int i = (attributeCount << 2) - 4; i >= 0; i -= 4) 
      if (attribs[i].name == name
          && (namespace == null || attribs[i].nsUri == namespace))
        return attribs[i].value;
    }

    return null;
  }

  public function getEventType(): XmlEventType {
    return type;
  }

  public function next() {

    txtPos = 0;
    whitespace = true;
    var minType = 9999;
    token = false;

    do {
      nextImpl();
      if (type < minType)
        minType = type;
      //	    if (curr <= TEXT) type = curr; 
    }
    while (minType > (ENTITY_REF :Int) // ignorable
        || (minType >= (TEXT :Int) && peekType() >= (TEXT: Int)));

    type = minType;
    if (type > (TEXT: Int))
      type = TEXT;

    return type;
  }

  public function nextToken() {

    whitespace = true;
    txtPos = 0;

    token = true;
    nextImpl();
    return type;
  }

  //
  // utility methods to make XML parsing easier ...

  public function nextTag() {

    next();
    if (type == TEXT && whitespace)
      next();

    if (type != END_TAG && type != START_TAG)
      exception("unexpected type");

    return type;
  }

  public function require(type: Int, namespace: String, name: String): Void {

    // if (this.type == TEXT && whitespace && type != TEXT) next(); // was valid in 1.0 now it's not.

    if (type != this.type
        || (namespace != null && !(namespace == getNamespace()))
        || (name != null && !(name == getName())))
      exception(
          "expected: " + XmlEventType.getName(type) + " {" + namespace + "}" + name);
  }

  public function nextText() {
    if (type != START_TAG)
      exception("precondition: START_TAG");

    next();

    var result: String;

    if (type == TEXT) {
      result = getText();
      next();
    }
    else
      result = "";

    if (type != END_TAG)
      exception("END_TAG expected");

    return result;
  }

  public function setFeature(feature: String, value: Bool): Void {
    if (XmlConstants.FEATURE_PROCESS_NAMESPACES == feature)
      processNsp = value;
    else if (isProp(feature, false, "relaxed"))
      relaxed = value;
    else
      exception("unsupported feature: " + feature);
  }

  public function setProperty(property: String, value: Dynamic): Void {
    if(isProp(property, true, "location"))
      location = value;
    else
      throw new XmlPullParserException("unsupported property: " + property);
  }


  public function readText(): String {
    if (type != TEXT) return "";

    var result = getText();
    next();
    return result;

  }

  /**
   * Skip sub tree that is currently porser positioned on.
   * <br>NOTE: parser must be on START_TAG and when funtion returns
   * parser will be positioned on corresponding END_TAG. 
   */

  //	Implementation copied from Alek's mail... 

  public function skipSubTree(): Void {
    require(START_TAG, null, null);
    var level = 1;
    while (level > 0) {
      var eventType = next();
      if (eventType == END_TAG) {
        --level;
      }
      else if (eventType == START_TAG) {
        ++level;
      }
    }
  }
}

private typedef Namespace = {
  var nsUri: String;
  var prefix: String;
}

private typedef Attribute = {
  > Namespace,
  var name: String;
  var value: String;
}
