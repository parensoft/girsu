/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

/* Copyright (c) 2002,2003, Stefan Haustein, Oberhausen, Rhld., Germany
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The  above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE. */


package parensoft.girsu;

import haxe.io.Output;

import xmlpull.*;

import parensoft.eridu.io.OutputWriter;
import parensoft.eridu.io.Writer;
import parensoft.eridu.CharsetRegistry;

import parensoft.girsu.util.Range;

using StringTools;

class Serializer implements XmlSerializer
{
    
    //    static final String UNDEFINED = ":";
    
    @:isVar
    private var writer(get, set): Writer; // FIXME null check on every access
    
    private var pending: Bool = false;
    private var auto: Int = 0;
    private var depth: Int = 0;
    
    private var elementStack: Array<String> = new Array<String>();
    //nsp/prefix/name
    private var nspCounts: Array<Int> = new Array<Int>();
    private var nspStack: Array<String> = new Array<String>();
    //prefix/nsp; both empty are ""
    private var indent: Array<Bool> = new Array<Bool>();
    private var unicode: Bool = false;
    private var encoding: String;

    @:allow(parensoft.girsu.ParserFactory)
    private function new() {}

    private function get_writer() {
      if (writer == null) {
        throw new XmlPullParserException("no writer set!");
      }
      return writer;
    }

    private function set_writer(aWriter: Writer) {
      writer = aWriter;
      return writer;
    }
    
    private function completeStartTag(close: Bool): Void
    {
        if (!pending)
            return;
        
        depth++;
        pending = false;
        
        indent[depth] = indent[depth - 1];
        
        for (i in Range.from(nspCounts[depth - 1]).to(nspCounts[depth])) {
            writer.write(' ');
            writer.write("xmlns");
            if ("" != nspStack[i * 2]) {
                writer.write(':');
                writer.write(nspStack[i * 2]);
            }
            else if ("" == getNamespace() && "" != nspStack[i * 2 + 1])
                throw new XmlPullParserException("Cannot set default namespace for elements in no namespace");
            writer.write("=\"");
            writeEscaped(nspStack[i * 2 + 1], "\"".code);
            writer.write('"');
        }
        
        nspCounts[depth + 1] = nspCounts[depth];
        //   nspCounts[depth + 2] = nspCounts[depth];
        
        writer.write(close ? " />" : ">");
    }
    
    private function writeEscaped(s: String, quot: Int): Void {
        
        for (i in Range.from(0).to(s.length)) {
            var c: Int = s.charCodeAt(i);
            switch (c)
            {
              case "\n".code | "\r".code | "\t".code:
                if(quot == -1)
                  writer.writeChar(c);
                else
                  writer.write("&#"+(c)+';');
              case "&".code :
                writer.write("&amp;");
              case ">".code :
                writer.write("&gt;");
              case "<".code :
                writer.write("&lt;");
              case "\"".code | "'".code :
                if (c == quot || quot == -1)
                {
                  writer.write(
                      c == "\"".code ? "&quot;" : "&apos;");
                }
              default :
                if (c < 32) throw new XmlPullParserException('control character $c not allowed');

                if (c >= " ".code && c !="@".code && (c < 127 || unicode))
                  writer.writeChar(c);
                else
                  writer.write("&#" + c + ";");

            }
        }
    }
    
    /*
    	private final void writeIndent() throws IOException {
    		writer.write("\r\n");
    		for (int i = 0; i < depth; i++)
    			writer.write(' ');
    	}*/
    
    public function docdecl(dd: String): Void
    {
        writer.write("<!DOCTYPE");
        writer.write(dd);
        writer.write(">");
    }
    
    public function endDocument(): Void
    {
        while (depth > 0) {
            endTag(
                elementStack[depth * 3 - 3],
                elementStack[depth * 3 - 1]);
        }
        flush();
    }
    
    public function entityRef(name: String): Void
    {
        completeStartTag(false);
        writer.write('&');
        writer.write(name);
        writer.write(';');
    }
    
    public function getFeature(name: String): Bool
    {
        //return false;
        return (
            "http://xmlpull.org/v1/doc/features.html#indent-output" == name)
            ? indent[depth]
            : false;
    }
    
    public function getPrefix(namespace: String, create: Bool): String {
        return doGetPrefix(namespace, false, create);
    }
    
    private function doGetPrefix(
        namespace: String,
        includeDefault: Bool,
        create: Bool): String
        
        {
        
        for (i in Range.from(nspCounts[depth + 1] * 2 - 2).by(-2).to(0)) {
            if (nspStack[i + 1] == namespace
                && (includeDefault || nspStack[i] != "")) {
                var cand: String = nspStack[i];
                for (j in Range.from(i + 2).to(nspCounts[depth + 1] * 2)) {
                    if (nspStack[j] == cand) {
                        cand = null;
                        break;
                    }
                }
                if (cand != null)
                    return cand;
            }
        }
        
        if (!create)
            return null;
        
        var prefix: String;
        
        if ("" == namespace)
            prefix = "";
        else
        {
            do {
                prefix = "n" + (auto++);
                for (i in Range.from(nspCounts[depth + 1] * 2 - 2).by(-2).to(0)) {
                    if (prefix == nspStack[i]) {
                        prefix = null;
                        break;
                    }
                }
            }
            while (prefix == null);
        }
		
		var p: Bool = pending;
		pending = false;
        setPrefix(prefix, namespace);
        pending = p;
        return prefix;
    }
    
    public function getProperty(name: String): Dynamic {
        throw new XmlPullParserException("Unsupported property");
    }
    
    public function ignorableWhitespace(s: String): Void {
        text(s);
    }
    
    public function setFeature(name: String, value: Bool): Void {
        if ("http://xmlpull.org/v1/doc/features.html#indent-output"
             == name)
            {
            indent[depth] = value;
        }
        else
            throw new XmlPullParserException("Unsupported Feature");
    }
    
    public function setProperty(name: String, value: Dynamic): Void
    {
        throw new XmlPullParserException(
            "Unsupported Property:" + value);
    }
    
    public function setPrefix(prefix: String, namespace: String): Void
        
        {
        
        completeStartTag(false);
        if (prefix == null)
            prefix = "";
        if (namespace == null)
            namespace = "";
        
        var defined: String = doGetPrefix(namespace, true, false);
        
        // boil out if already defined
        
        if (prefix == defined)
            return;
        
        var pos: Int = (nspCounts[depth + 1]++) << 1;
        
        nspStack[pos++] = prefix;
        nspStack[pos] = namespace;
    }
    
    public function setOutput(os: Output, ?anEncoding: String = "UTF-8"): Void {
        if (anEncoding == null)
            throw new XmlPullParserException("null encoding");
        nspCounts[0] = 2;
        nspCounts[1] = 2;
        nspStack[0] = "";
        nspStack[1] = "";
        nspStack[2] = "xml";
        nspStack[3] = "http://www.w3.org/XML/1998/namespace";
        pending = false;
        auto = 0;
        depth = 0;
        if (os == null) {
          encoding = null;
          unicode = false;
        }
        else {
          writer = new OutputWriter(os, CharsetRegistry.byName(anEncoding));
          encoding = anEncoding;
          unicode = encoding.toLowerCase().startsWith("utf");
        }
    }
    
    public function startDocument(
        encoding: String,
        standalone: Null<Bool>): Void {
        writer.write("<?xml version='1.0' ");
        
        if (encoding != null)
        {
            this.encoding = encoding;
            if (encoding.toLowerCase().startsWith("utf"))
                unicode = true;
        }
        
        if (this.encoding != null)
        {
            writer.write("encoding='");
            writer.write(this.encoding);
            writer.write("' ");
        }
        
        if (standalone != null)
        {
            writer.write("standalone='");
            writer.write(
                standalone ? "yes" : "no");
            writer.write("' ");
        }
        writer.write("?>");
    }
    
    public function startTag(namespace: String, name: String): Void {
        completeStartTag(false);
        
        //        if (namespace == null)
        //            namespace = "";
        
        if (indent[depth])
        {
            writer.write("\r\n");
            for (i in Range.from(0).to(depth))
                writer.write("  ");
        }
        
        var esp: Int = depth * 3;
        
        var prefix: String =
            namespace == null
                ? ""
                : doGetPrefix(namespace, true, true);
        
        if ("" == namespace) {
            for (i in Range.from(nspCounts[depth]).to(nspCounts[depth + 1])) {
                if ("" == nspStack[i * 2] && "" != nspStack[i * 2 + 1]) {
                    throw new XmlPullParserException("Cannot set default namespace for elements in no namespace");
                }
            }
        }
        
        elementStack[esp++] = namespace;
        elementStack[esp++] = prefix;
        elementStack[esp] = name;
        
        writer.write('<');
        if ("" != prefix) {
            writer.write(prefix);
            writer.write(':');
        }
        
        writer.write(name);
        
        pending = true;
        
    }
    
    public function attribute(namespace: String, name: String, value: String): Void {
        if (!pending)
            throw new XmlPullParserException("illegal position for attribute");
        
        //        int cnt = nspCounts[depth];
        
        if (namespace == null)
            namespace = "";
        
        //		depth--;
        //		pending = false;
        
        var prefix = "" == namespace
                ? ""
                : doGetPrefix(namespace, false, true);
        
        //		pending = true;
        //		depth++;
        
        /*        if (cnt != nspCounts[depth]) {
                    writer.write(' ');
                    writer.write("xmlns");
                    if (nspStack[cnt * 2] != null) {
                        writer.write(':');
                        writer.write(nspStack[cnt * 2]);
                    }
                    writer.write("=\"");
                    writeEscaped(nspStack[cnt * 2 + 1], '"');
                    writer.write('"');
                }
                */
        
        writer.write(' ');
        if ("" != prefix) {
            writer.write(prefix);
            writer.write(':');
        }
        writer.write(name);
        writer.write('=');
        var q = value.indexOf('"') < 0 ? "\"".code : "'".code;
        writer.writeChar(q);
        writeEscaped(value, q);
        writer.writeChar(q);
    }
    
    public function flush(): Void
    {
        completeStartTag(false);
        writer.flush();
    }
    /*
    	public void close() throws IOException {
    		completeStartTag();
    		writer.close();
    	}
    */
    public function endTag(namespace: String, name: String): Void {
        
        if (!pending)
            depth--;
        //        if (namespace == null)
        //          namespace = "";
        
        if ((namespace == null
            && elementStack[depth * 3] != null)
            || (namespace != null
                && namespace != elementStack[depth * 3])
            || elementStack[depth * 3 + 2] != name)
            throw new XmlPullParserException("</{"+namespace+"}"+name+"> does not match start");
        
        if (pending)
        {
            completeStartTag(true);
            depth--;
        }
        else
        {
            if (indent[depth + 1]) {
                writer.write("\r\n");
                for (i in Range.from(0).to(depth))
                    writer.write("  ");
            }
            
            writer.write("</");
            var prefix: String = elementStack[depth * 3 + 1];
            if ("" != prefix) {
                writer.write(prefix);
                writer.write(':');
            }
            writer.write(name);
            writer.write('>');
        }
        
        nspCounts[depth + 1] = nspCounts[depth];
    }
    
    public function getNamespace(): String {
        return getDepth() == 0 ? null : elementStack[getDepth() * 3 - 3];
    }
    
    public function getName(): String {
        return getDepth() == 0 ? null : elementStack[getDepth() * 3 - 1];
    }
    
    public function getDepth(): Int {
        return pending ? depth + 1 : depth;
    }
    
    public function text(text: String): Void {
        completeStartTag(false);
        indent[depth] = false;
        writeEscaped(text, -1);
    }
    
    public function cdsect(data: String): Void {
        completeStartTag(false);
        writer.write("<![CDATA[");
        writer.write(data);
        writer.write("]]>");
    }
    
    public function comment(comment: String): Void {
        completeStartTag(false);
        writer.write("<!--");
        writer.write(comment);
        writer.write("-->");
    }
    
    public function processingInstruction(pi: String): Void {
        completeStartTag(false);
        writer.write("<?");
        writer.write(pi);
        writer.write("?>");
    }
}
