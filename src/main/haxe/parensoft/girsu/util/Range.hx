/****
* Copyright (c) 2015 Parensoft.NET
* 
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
* 
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
* 
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
* OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
* THE SOFTWARE.
* 
****/

package parensoft.girsu.util;

/**
 * ...
 * @author Parensoft.NET
 */
class Range {
    
    private var curr: Int;
    private var step: Int;
    private var end: Int;
    
    private function new(aStart: Int, aEnd: Int, ?aStep: Int = 1) {
        curr = aStart;
        end = aEnd;
        step = aStep;
    }
    
    public function hasNext() {
        return if (step > 0) curr < end else curr > end;
    }

    public function next() {
        var r = curr;
        curr += step;
        return r;
    }
    
    
    public static function from(aStart: Int) {
        var fBy = function(aBy: Int) {
              return {
                  to: function(aTo: Int) {
                      return new Range(aStart, aTo, aBy);
                  },
                  
                  toIncl: function(aTo: Int) {
                      return new Range(aStart, aTo + aBy, aBy);
                  }
              };  
            };
        
        return {
            by: fBy,
            
            to: function(aTo: Int) {
                return fBy(1).to(aTo);
            },
            
            toIncl: function(aTo: Int) {
                return fBy(1).toIncl(aTo);
            }
        };
    }
    
    
}

