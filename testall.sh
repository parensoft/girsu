#!/bin/sh

rm -fr bin/*

haxe girsu-tests.hxml

export XMLPULL_FACTORY=parensoft.girsu.ParserFactory

echo
echo '****************** TESTING NEKO ******************'
echo
neko bin/girsu.n


echo
echo '****************** TESTING JAVA ******************'
echo
java -jar bin/Main.jar


echo
echo '****************** TESTING C# ******************'
echo
mono bin/bin/Main.exe


echo
echo '****************** TESTING C++ ******************'
echo
bin/Main-debug


echo
echo '****************** TESTING PHP ******************'
echo
php bin/index.php


