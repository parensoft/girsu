# XML Pull Parser for the Haxe language (a port of kxml2 from Java)

This is a port of the kxml2 parser to the Haxe language. It is written to conform to the public domain XML Pull API adapted from Java. The API and the parser itself are as similar as possible to the Java ones. Some changes to the API were however necessary as e.g. Haxe does not support overloading methods thus forcing the renaming of some.

Unlike most (all?) other parsers available for Haxe this is only a parser (and later a serializer will be added), and it does not require the entire XML to be loaded into memory for parsing. It parses XML while reading from a ```parensoft.eridu.Reader``` or a ```haxe.io.Input``` (which is assumed to be UTF-8-encoded) and it is up to the user to find the data they need and build appropriate data structures.

The API itself is in the public domain, and is available from [Haxelib](http://lib.haxe.org/p/xmlpull).

The parser has been tested with XML Pull API tests. So far it passes all the tests except those in files TestSimpleToken and TestEntityReplacement, as these require entity declaration parsing, not supported by the parser now.

To run the tests you need to install [utest](http://lib.haxe.org/p/utest) and [xmlpull-tests](https://bitbucket.org/parensoft/xmlpull-tests).

### Version history
2015.01.29 First released

### Roadmap:
* Add entity replacement code, pass all tests
* Port serializer
* Implement the higher level API as soon as it's available
